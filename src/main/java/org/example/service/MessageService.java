package org.example.service;

import com.google.gson.Gson;
import org.example.dao.ChatDao;
import org.example.dao.MessageDao;
import org.example.dao.UserDao;
import org.example.dto.Message;
import org.example.dto.UserToken;
import org.example.model.User;
import org.example.toJson.MessageJson;
import org.example.toJson.RoomsJson;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MessageService {
    private static Gson gson = new Gson();
    static SimpleDateFormat format = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    static User user;
    public static String commandHandle(Message message) {
        ChatDao chatDao = new ChatDao();
        UserDao userDao = new UserDao();
        MessageDao messageDao = new MessageDao();
        switch (message.getCommand()) {
            case ROOMS: {
                try {
                    ArrayList<RoomsJson> chatPublic = (ArrayList<RoomsJson>) chatDao.readChatPublic().stream().map(
                            e -> new RoomsJson(
                                    e.getIs_private(),
                                    e.getId_chat(),
                                    e.getNameChat(),
                                    null,
                                    e.getCreated_at(),
                                    e.getUpdate_at(),
                                    null)
                    ).collect(Collectors.toList());

                    return "{\"command\":\"ROOMS\", \"data\":" + new Gson().toJson(chatPublic) + "}";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case USERS:
//                try  {
//                    List<User> users = userDao.read();
//                    ArrayList<PrivateChatJson> privateChatJsons = (ArrayList<PrivateChatJson>) users.stream()
//                            .map(e->new PrivateChatJson(
//                                    e.getId_user(),
//                                    e.getFirstName(),
//                                    e.getLogin(),
//                                    e.getOnline(),
//                                    e.getAvatar())).collect(Collectors.toList());
//                    System.out.println(new Gson().toJson(privateChatJsons));
//
//                    return "{\"command\":\"USERS\", \"users\":" + new Gson().toJson(privateChatJsons) + "}";
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                break;
            case CURRENT_USER:
                 user = userDao.getById(Integer.parseInt(message.getData().getUserId()));
                return "{\"command\":\"CURRENT_USER\", \"data\":"
                        + gson.toJson(new UserToken(
                        String.valueOf(user.getId_user()),
                        user.getFirstName(),
                        user.getLogin(),
                        user.getFirstName())) + "}";

            case JOINROOM:
                try {
                    List<org.example.model.Message> messages = messageDao.readMessageByChat(chatDao.getById(Integer.parseInt(message.getData().getRoomId())).getNameChat());

                    ArrayList<MessageJson> messageJsons = (ArrayList<MessageJson>) messages.stream()
                            .map(e -> new MessageJson(e.getText(), e.getUser(), e.getTime())).collect(Collectors.toList());
                    System.out.println(new Gson().toJson(messageJsons));
                    return "{\"command\":\"JOINROOM\", \"data\":" + new Gson().toJson(messageJsons) + "}";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            case SENDMESSAGE:
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    messageDao.save(new org.example.model.Message(0,
                            message.getData().getMessage(),
                            chatDao.getById(Integer.parseInt(message.getData().getRoomId())).getNameChat(),
                            format.format(new Date()),
                            user.getLogin()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }


        return "No command";
    }
}
