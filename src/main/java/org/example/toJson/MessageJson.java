package org.example.toJson;

public class MessageJson {
    private String message;
    private String user;
    private String timestamp;

    public String getMessage() {
        return message;
    }

    public String getUser() {
        return user;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public MessageJson(String message, String user, String timestamp) {
        this.message = message;
        this.user = user;
        this.timestamp = timestamp;
    }
}
