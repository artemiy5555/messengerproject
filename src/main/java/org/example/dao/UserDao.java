package org.example.dao;


import org.example.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.example.util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    public void save(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(user);
        tx1.commit();
        session.close();
    }

    public void update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE User WHERE id_user = :id")
                .setParameter("id", id)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    public List read() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<User> list =
                (ArrayList<User>) session
                        .createQuery("FROM User")
                        .list();
        session.close();
        return list;
    }

    public boolean contains(String login, String pass) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<User> user =  session.createQuery("FROM User WHERE login = :login and password=:pass")
                        .setParameter("login", login)
                .setParameter("pass",pass)
                .list();
        session.close();
        return user.size() == 1;

    }
    public User getUserByLoginAndPass(String login, String pass) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<User> user =  session.createQuery("FROM User WHERE login = :login and password=:pass")
                .setParameter("login", login)
                .setParameter("pass",pass)
                .list();
        session.close();
        return user.get(0);
    }

    public User getById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession()
                .get(User.class, id);
    }

    public void addChatToUser(int id_user,int id_chat){
        User user = getById(id_user);
        user.createChat(new ChatDao().getById(id_chat));
        update(user);
    }

    public void removeChatToUser(int id_user,int id_chat){
        User user = getById(id_user);
        user.removeChat(new ChatDao().getById(id_chat));
        update(user);
    }

    public void addReting(int id_user){
        User user = getById(id_user);
        user.setRating(user.getRating()+1);
        update(user);
    }




}
