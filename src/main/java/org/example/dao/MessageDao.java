package org.example.dao;

import org.example.model.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.example.util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class MessageDao {

    public void save(Message message) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(message);
        tx1.commit();
        session.close();
    }

    public void update(Message message) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(message);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Message WHERE id_message = :id")
                .setParameter("id", id)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    public List<Message> readMessageByChat(String nameOfChat) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Message> list =
                (ArrayList<Message>) session
                        .createQuery("FROM Message where nameOfChat=:nameOfChat order by time asc ")
                        .setParameter("nameOfChat", nameOfChat)
                        .list();
        session.close();
        return list;
    }
}
